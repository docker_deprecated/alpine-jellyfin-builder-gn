# alpine-jellyfin-builder-gn

#### [alpine-x64-jellyfin-builder-gn](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-jellyfin-builder-gn/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64build/alpine-x64-jellyfin-builder-gn/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64build/alpine-x64-jellyfin-builder-gn/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64build/alpine-x64-jellyfin-builder-gn/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-jellyfin-builder-gn)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-jellyfin-builder-gn)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : gn (Build Image)
    - GN is a meta-build system that generates build files for Ninja.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

