ARG BUILD_ARCH=x64

FROM forumi0721alpine${BUILD_ARCH}build/alpine-${BUILD_ARCH}-buildbase:latest as gn

LABEL maintainer="forumi0721@gmail.com"

COPY local/. /usr/local/

#RUN ["docker-build-start"]

RUN ["docker-init"]

#RUN ["docker-build-end"]



FROM forumi0721/busybox-${BUILD_ARCH}-base:latest

LABEL maintainer="forumi0721@gmail.com"

COPY --from=gn /output /output

